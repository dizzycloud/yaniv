var maxPlayersPerGame = 2;

function Deck() {

    //initialization of the cards array        
    this.cards = new Array();

    for (i = 0; i < 54; i++) {
        this.cards[i] = i; // init cards
    }

};


Deck.prototype.pickRandomCard = function () {
  if(this.cards.length < 1){
    console.log("no more cards");
    return -1;
  }
    var cardPlace = Math.floor(Math.random() * (this.cards.length));
    if(cardPlace == this.cards.length){
      return 54;
    }
    var card = this.cards[cardPlace];
    this.cards.splice(cardPlace,1);
    return card;
};

function Player(nickName){
    this.nickName = nickName;
    this.inGame = false;
    this.gamePoints = 0;
    this.turnToPlay = false;
    this.gamePoints = 0;
};

function Game(id, gamesArray){
    this.id = id;
    this.players = [];
    this.deck = new Deck();
}

Game.prototype.addPlayer = function (player) {
    if(this.players.length < maxPlayersPerGame){
        this.players.push(player);
    }
}

function GamesArray(){
    this.Games = [];
}
GamesArray.prototype.addGame = function (game) {
        this.Games.push(game);
}

//module.exports.Deck = Deck;
module.exports.Player = Player;
module.exports.Game = Game;
module.exports.GamesArray = GamesArray;