var yanivModule = angular.module('yanivGameModule' ,['ui.router']);

yanivModule.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'templates/home.html'
        })
        .state('game', {
            url: '/game',
            templateUrl: 'templates/game.html',
            controller: 'yanivPlayerCtrl'
        });
})


yanivModule.run(function($templateCache){
    $templateCache.put('playerCard.html','<div class="{{card.crd}}{{card.figure}} card selected{{card.isSelected}}" ' +
                        'ng-click="selectCard(card)">{{card.crd}}{{card.figure}}' +
                    '</div>');
    $templateCache.put('garbageCard.html','<div class="{{card.crd}}{{card.figure}} card" ' +
                            'ng-click="takeCardFromGarbage(card)">{{card.crd}}{{card.figure}}' +
                       '</div>');
    $templateCache.put('deck.html','<div class="deck card" ng-click="takeCard()"></div>');
})
/*var maxPointsToYaniv = yanivModule.factory('maxPointsToYaniv', function(){
      return {
          maxPoints: function(maxPoints) {
              return (maxPoints);
          }
      };
});

var injector = angular.injector(['yanivGameModule', 'ng']);

var maxPointsToYaniv = injector.get('maxPointsToYaniv');  */

yanivModule.directive('playercard', function ($templateCache) {
    return{
        restrict:"E",
        templateUrl: "playerCard.html",
        link: function(scope){
            scope.selectCard = function(card){
                card.isSelected = !card.isSelected;
            }
        }
    }
});

yanivModule.directive('deck', function ($templateCache) {
    return{
        restrict:"E",
        templateUrl: "deck.html",
        link: function(scope){
            scope.takeCard = function(){
                var selectedSortedCardsArray = scope.getSelectedSortedCards();
                var canThrow = scope.checkIfSelectedCardsAreLegal(selectedSortedCardsArray);
                if(canThrow){
                    scope.getRandomCardFromDeck();
                    scope.throwTheGarbage(selectedSortedCardsArray);
                    scope.deselectHandCards();
                }
            }
        }
    }
});

yanivModule.directive('garbagecard', function ($templateCache) {
    return{
        restrict:"E",
        templateUrl: "garbageCard.html",
        link: function(scope){
            scope.takeCardFromGarbage = function(card){
                var selectedSortedCardsArray = scope.getSelectedSortedCards();
                var canThrow = scope.checkIfSelectedCardsAreLegal(selectedSortedCardsArray);
                if(canThrow){
                    var canTake = scope.checkIfSelectedGarbageCardIsLegal(card);
                    if(canTake){
                        scope.hand.push(card);
                        scope.throwTheGarbage(selectedSortedCardsArray);
                        scope.deselectHandCards();
                    }
                }
            }
        }
    }
});




function yanivPlayerCtrl($scope, $http) {

    $scope.maxNumberOfJokersInPack = 2;

    $scope.maxPointsToYaniv = 7;

    $scope.opponentNumberOfCards =  3;

    $scope.gamePoints =  63;

    $scope.opponentGamePoints =  80;

    //TODO add http request
    $scope.getPlayerCardsFromServer = function(){
        $http({method: 'POST', url: '/getFirstHand'}).
          success(function(data, status) {
            for (var i = 0; i < data.cards.length; i++){
                $scope.hand.push(new Card(data.cards[i]));
            }
          }).
          error(function(data, status) {
            console.log("an error occurred");
          });   
    }

    $scope.getFirstGarbageCardFromServer = function(){
        $scope.garbage = [
        ];
        $http({method: 'POST', url: '/getcard'}).
          success(function(data, status) {
            if ( data == -1 ){
                console.log("no more cards");
                return;
            }
            $scope.garbage.push(new Card(data));
          }).
          error(function(data, status) {
            console.log("an error occurred in getFirstGarbageCardFromServer");
          });        
    }

    $scope.hand = [
    ];


    $scope.garbage = [
    ];

    $scope.getOpponentNumberOfCards = function(){
        return new Array($scope.opponentNumberOfCards);
    };

    $scope.getHandPoints = function(){
        var myPoints = 0;
        for(var i = 0; i < $scope.hand.length; i++){
            myPoints += $scope.hand[i].value;
        }
        return myPoints;
    }

    $scope.checkIfSelectedCardsAreLegal = function(selectedSortedCardsArray){
        var isEmpty = $scope.checkIfNoCardsWereSelected(selectedSortedCardsArray);
        if(isEmpty){
            return false;
        }
        else{
            var areIdenticalCardsOrOneCard = $scope.checkIfSelectedCardsAreIdentical(selectedSortedCardsArray);
            if(areIdenticalCardsOrOneCard){
                return true;
            }
            else{
                var isStraight = $scope.checkIfStraight(selectedSortedCardsArray);
                if (isStraight){
                        return true;
                }
                return false
            }
        }
    }

    $scope.canYaniv = function(){
        if ($scope.getHandPoints() > $scope.maxPointsToYaniv){

            return false;
        }
        else{

            return true;
        }
    }

    $scope.getSelectedSortedCards = function(){
        var selectedSortedCards = [];
        $scope.hand.forEach(function(card){
            if(card.isSelected){
                selectedSortedCards.push(card);
            }
        })
        selectedSortedCards.sort(function(cardA, cardB) {
            return cardA.crd - cardB.crd;
        })
        return selectedSortedCards;
    }

    $scope.checkIfNoCardsWereSelected = function(selectedSortedCardsArray){
        if (selectedSortedCardsArray.length === 0){
            return true;
        }
        else{
            return false;
        }
    }

    $scope.checkIfSelectedCardsAreIdentical = function(selectedSortedCardsArray){
        var cardCrd = 0;
        for(var i = 0; i < selectedSortedCardsArray.length; i++){
            //init cardCrd (or joker)
            if(cardCrd === 0){
                cardCrd = selectedSortedCardsArray[i].crd;
            }
            else{
                if(selectedSortedCardsArray[i].crd != cardCrd){
                    return false;
                }
            }
        }
        return true;
    }
    $scope.checkIfStraight = function(selectedSortedCardsArray){
//        check if at least 3 cards
        if(selectedSortedCardsArray.length < 3){
            return false;
        }
        var isFlush = $scope.checkIfFlush(selectedSortedCardsArray);
        if(isFlush){
            var jokers = [];
//            move the jokers from the array if exist
            for(var joker = 0; joker < ($scope.maxNumberOfJokersInPack); joker++){
                if(selectedSortedCardsArray[0].crd === 0){
                    jokers.push(selectedSortedCardsArray[0]);
                    selectedSortedCardsArray.shift();
                }
                else{
                    break;
                }
            }
//            check if straight
            for(var i = 0; i < (selectedSortedCardsArray.length - 1); i++){
                var difference = selectedSortedCardsArray[i+1].crd - selectedSortedCardsArray[i].crd;
                var numberOfJokersToAdd = 0;
                for(var j = difference; j > 1; j--){
                    numberOfJokersToAdd++;
                }
//                push the jokers to the right place in the array if needed
                for(var jokerToAdd = 0; jokerToAdd < numberOfJokersToAdd; jokerToAdd++){
                    if(jokers.length < 1){
                        return false;
                    }
                    else{
                        var lastJokerIndex = jokers.length-1;
                        var jokerCard = jokers[lastJokerIndex];
                        jokers.pop();
                        selectedSortedCardsArray.splice((i + 1), 0, jokerCard);
                        i++;
                    }
                }

            }
//            push the jokers that weren't in use to the end of the array
            for(var jokerToAddToEnd = 0; jokerToAddToEnd < jokers.length; jokerToAddToEnd++){
                selectedSortedCardsArray.push(jokers[jokerToAddToEnd]);
            }
            return true;
        }
        else{
            return false;
        }
    }
    $scope.checkIfFlush = function(selectedSortedCardsArray){
        var cardFigure = "all";
        for(var i = 0; i < selectedSortedCardsArray.length; i++){
            //init cardFigure (or joker)
            if(cardFigure === "all"){
                cardFigure = selectedSortedCardsArray[i].figure;
            }
            else{
                if(selectedSortedCardsArray[i].figure != cardFigure){
                    return false;
                }
            }
        }
        return true;
    }

    $scope.getRandomCardFromDeck = function(){
        $http({method: 'POST', url: '/getcard'}).
          success(function(data, status) {
            if ( data == -1 ){
                console.log("no more cards");
                return;
            }
            $scope.hand.push(new Card(data));
          }).
          error(function(data, status) {
            console.log("an error occurred");
          });        
    }

    $scope.checkIfSelectedGarbageCardIsLegal = function(card){
        var areIdentical = $scope.checkIfSelectedCardsAreIdentical($scope.garbage);
        if(areIdentical){
            $scope.takeGarbageCard(card.cardIndex);
            return true;
        }
        else{
            if($scope.garbage[0] === card){
                $scope.takeGarbageCard(card.cardIndex);
                return true;
            }
            else{
                var legalCrd = 0;
                for(var i = $scope.garbage.length - 1; i > 0; i--){
                    if(legalCrd === 0){
                        if($scope.garbage[i] === card){
                            $scope.takeGarbageCard(card.cardIndex); 
                            return true;
                        }
                        else{
                            legalCrd = $scope.garbage[i].crd;
                        }
                    }
                    else{
                        return false;
                    }
                }
            }
        }
    }

    $scope.takeGarbageCard = function(cardIndex){
        $http.post("/selectedGarbageCard", cardIndex)
        // .success(function(data, status, headers, config) {
        //     //$scope.data = data;
        // }).error(function(data, status, headers, config) {
        //     //$scope.status = status;
        // });
    }

    $scope.deselectHandCards= function(){
        $scope.hand.forEach(function(card){
            card.isSelected = false;
        })
    }

    $scope.throwTheGarbage = function(selectedSortedCardsArray){
        selectedSortedCardsArray.forEach(function(card){
            var cardIndex = $scope.hand.indexOf(card);
            $scope.hand.splice(cardIndex, 1);
        })
        $scope.garbage = selectedSortedCardsArray;
    }

    $scope.doTheYaniv = function(){
        var handPoints = $scope.getHandPoints();
        if(handPoints <= $scope.maxPointsToYaniv){
//      TODO send to server
            console.log(handPoints + " yaniv");
        }
    }


    //init player cards
    $scope.getPlayerCardsFromServer();
    $scope.getFirstGarbageCardFromServer();

    /*
    $scope.takeCard = function(){
        $scope.hand.pop();
        $scope.gamePoints = 30;
    }

    $scope.$watch('gamePoints', function() { console.log('watch!'); });   */
}
