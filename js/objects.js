
/**
 * Class Card
 * @param cardindx {number} between 0 and 53, which states the card index in a full dec
 * @field crd {number} card nunber(1-13, 0-joker)
 * @field figure {string} enum for card figure (type) ["H", "C", "S", "D"] Heart/Club/Spade/Diamond
 * @field value {number} the card value (points) in Yaniv
 * @field selected {bool} whether the card is selected
 */
function Card(cardindx) {
    this.cardIndex = cardindx;
    this.crd = this.getCardCrd(cardindx);
    this.figure = this.getCardFigure(cardindx);
    // royal family member (King, Queen and Jack) count as 10
    this.value = Math.min((this.crd), 10);
    this.isSelected = false;
} // end of class Card

Card.prototype.getCardCrd = function (cardindx) {
    if (cardindx == 0 || cardindx == 53) {
        // joker
        return 0;
    }

    else {
        var cardCrd = cardindx % 13;
        if (cardCrd == 0) {
            return 13;
        }
        else {
            return cardCrd;
        }
    }
};

// calc figure
Card.prototype.getCardFigure = function (cardindx) {
    if (cardindx == 0 || cardindx == 53) {
        //joker
        return "all";
    }
    else{
        switch (Math.floor((cardindx - 1) / 13)) {
            case 0:
                return "H";
            case 1:
                return "C";
            case 2:
                return "D";
            case 3:
                return "S";
        }
    }
};